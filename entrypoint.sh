#!/bin/bash

# The coloring text is where below var used on it
RED="\033[0;31m"
GREEN="\033[0;32m"
NC="\033[0;0m"

# This is generated password when ${DB_ROOT_PASS} is empty
dbrootpass=$(pwgen -s -0 16 1)

set -eo pipefail

_term() {
    echo "Caught SIGTERM signal!"
    childs=$(pstree -p $child | grep -o [0-9]* | sort -r -g)
    for pid in $childs 
    do
	# wait $pid
	kill -- $pid 2>/dev/null
	echo "kill pid $pid"
    done
    exit 0;
}

trap _term SIGTERM SIGINT

init-file-cnf () {
    cat <<EOM > /etc/mysql/my.cnf
    [mysqld]
    default-time-zone=+00:00
    port=3306
    max_allowed_packet=16M

EOM
}

init-new-database () {

    # Install db on directory where my.cnf refer to
    echo -e "${GREEN}Installing database${NC}"
    mysql_install_db --datadir=${DB_DATA_DIR} --user=mysql

    # Start mysqld_safe
    echo -e "${GREEN}Start mysql server using mysqld_safe${NC}"
    mysqld_safe --log-error=${DB_LOG_FILE=} --user=mysql &

    # Check if mysqld is alive
    echo -e "${GREEN}Wait mysql server to answear${NC}"
    mysqladmin --silent --wait=30 ping || exit 1

    # Create root user with remote access anywhere using password supplied above
    if [ "${DB_ROOT_PASS}" ]; then
	mysql -e "CREATE USER IF NOT EXISTS 'root'@'%' identified by '${DB_ROOT_PASS}';"
    else
	echo -e "${GREEN}Create root user using${RED} ${dbrootpass}${NC}"
	mysql -e "CREATE USER IF NOT EXISTS 'root'@'%' identified by '${dbrootpass}';"
    fi

    # Grant all privileges to root user created above and drop test database if exists
    mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
              DROP DATABASE IF EXISTS test;"

    # Then create database if ${DB_NAME} is supplied
    [ "${DB_NAME}" ] && mysql -e "CREATE DATABASE IF NOT EXISTS ${DB_NAME};"

    
    # Let remove anonymous user from database and flush privileges
    local anonymous
    anonymous=$(mysql -e "select user, host from mysql.user where user like '';")
    [ "$anonymous" ] && mysql -e "delete from mysql.user where user like ''; \
                                  flush privileges;"

    # Now. we apply any initial sql file like schema or database backup found on /tmp/initial.d directory
    for x in $(find /tmp/initial.d/ -iname '*.sql' -type f)
    do
	if [ ! "${DB_NAME}" ]; then
	    # If we found *.sql file and ${DB_NAME} not supplied then we assume the file contain 'USE [database_name];' command.
	    echo -e "${GREEN}Applying $x to database${NC}"
	    mysql -e "SOURCE $x;"
	else
	    # If we found *.sql file and ${DB_NAME} supplied then we source the file on that database.
	    echo -e "${GREEN}Applying $x to database ${RED}${DB_NAME}${NC}"
	    mysql -e "use ${DB_NAME}; SOURCE $x;"
	fi
    done
}


# Remove default my.cnf if exists
[ -e /etc/my.cnf ] && rm /etc/my.cnf

# Remove directory /etc/my.cnf.d if exists
[ -d /etc/my.cnf.d ] && rm -r /etc/my.cnf.d

# If not exists, write default my.cnf file on /etc/mysql directory
[ ! -e /etc/mysql/my.cnf ] && init-file-cnf

# Below is main execution of entrypoint
# Install database to location specified by my.cnf file or environment DB_DATA_DIR
if [ "$(ls -A ${DB_DATA_DIR})" ]; then
    echo -e "${GREEN}The data directory is exists, initiating existed data directory${NC}"
    mysqld_safe --log-error=${DB_LOG_FILE=} --user=mysql &
    echo -e "${GREEN} MariaDB is running${NC}"
    
else
    echo -e "${GREEN}The data directory is not exists, installing and initiating data directory${NC}"
    init-new-database
    echo -e "${GREEN} MariaDB is running${NC}"
fi

child=${!}

wait $child
exit $?;
