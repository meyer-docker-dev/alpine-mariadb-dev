#!/bin/bash

set -e

db-test (){
    # Let's create database test
    mysql -u root -h 127.0.0.1 -pMySQLPASSWORD -e "create database test;"

    # Create table on database test
    mysql -u root -h 127.0.0.1 -pMySQLPASSWORD -e "use test; \
    	     	     	       		       	   create table table_test \
						   ( id INT NOT NULL AUTO_INCREMENT, \
						     content VARCHAR(10) NOT NULL, \
						     PRIMARY KEY (id) );"

    # Insert some data
    mysql -u root -h 127.0.0.1 -pMySQLPASSWORD -e "use test; insert into table_test (content) values ('success');"

    # Query data we've insert before return exit 1 if nothing return
    local QUERY_RESULT
    QUERY_RESULT=$(mysql -u root -h 127.0.0.1 -pMySQLPASSWORD -e "use test; select * from table_test;")

    # Then we delete the database test
    mysql -u root -h 127.0.0.1 -pMySQLPASSWORD -e "drop database test;"

    echo "--------------------------"
    echo "Test result --------------"
    echo
    [ "${QUERY_RESULT}" ] && echo "${QUERY_RESULT}"|| exit 1

}

wait-and-loop (){
    sleep 5
    mysqladmin -u root -h 127.0.0.1 -pMySQLPASSWORD --silent ping  2> /dev/null
}

docker build -t test-mariadb-dev . && \
    docker run --rm \
	   -d \
	   -e DB_ROOT_PASS=MySQLPASSWORD \
	   -p 3306:3306 \
	   --name test-mariadb-dev \
	   test-mariadb-dev

sleep 5
wait-and-loop || wait-and-loop
echo
echo "--------------------------"
echo "connected to mysql server"
echo "running test"
echo 
db-test && docker stop test-mariadb-dev || exit 1
