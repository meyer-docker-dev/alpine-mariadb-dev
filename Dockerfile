FROM alpine

RUN apk update && apk add --no-cache mariadb mariadb-client pwgen bash

ENV DB_DATA_DIR="/var/lib/mysql"
ENV DB_LOG_FILE="/var/log/mysql/mysqld.log"

COPY ./entrypoint.sh /tmp/entrypoint.sh

RUN mkdir /run/mysqld && \
    chown -R mysql:mysql /run/mysqld && \
    chmod +x /tmp/entrypoint.sh && \
    mkdir /tmp/initial.d && mkdir /var/log/mysql && \
    chown -R mysql:mysql /var/log/mysql

EXPOSE 3306

# VOLUME ["/etc/mysql", "/var/lib/mysql", "/tmp/initial.d"]

# CMD ["entrypoint"]

ENTRYPOINT ["/tmp/entrypoint.sh"]