#!/bin/bash

set -e

db-test (){
    # Let's create database test
    mysql -u root -h docker -pMySQLPASSWORD -e "create database test;"

    # Create table on database test
    mysql -u root -h docker -pMySQLPASSWORD -e "use test; \
    	     	     	       		       	   create table table_test \
						   ( id INT NOT NULL AUTO_INCREMENT, \
						     content VARCHAR(10) NOT NULL, \
						     PRIMARY KEY (id) );"

    # Insert some data
    mysql -u root -h docker -pMySQLPASSWORD -e "use test; insert into table_test (content) values ('success');"

    # Query data we've insert before return exit 1 if nothing return
    local QUERY_RESULT
    QUERY_RESULT=$(mysql -u root -h docker -pMySQLPASSWORD -e "use test; select * from table_test;")

    # Then we delete the database test
    mysql -u root -h docker -pMySQLPASSWORD -e "drop database test;"

    echo "--------------------------"
    echo "Test result --------------"
    echo
    [ "${QUERY_RESULT}" ] && echo "${QUERY_RESULT}"|| exit 1

}

wait-and-loop (){
    echo "wait and loop"
    local PING_RESULT
    PING_RESULT=$(mysqladmin -u root -h docker -pMySQLPASSWORD --silent ping)
    local TRY_COUNTER
    TRY_COUNTER=0
    while [ ! "$PING_RESULT" ]; do
	TRY_COUNTER=$(( $TRY_COUNTER + 1 ))
	sleep 5
	PING_RESULT=$(mysqladmin -u root -h docker -pMySQLPASSWORD --silent ping)
	if (( $TRY_COUNTER == 5 )); then
	    echo $(docker exec test-alpine-mariadb cat /var/log/mysql/mysqld.log)
	    echo "Limit try reached"
	    exit 1
	else
	    echo "try $TRY_COUNTER times"
	fi
    done
}

wait-and-loop || wait-and-loop

echo
echo "--------------------------"
echo "connected to mysql server"
echo "running test"
echo 
db-test && mysqladmin -u root -h docker -pMySQLPASSWORD shutdown || exit 1
